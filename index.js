var W3CWebSocket = require('websocket').w3cwebsocket;
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;


app.get('/', function(req, res){
  res.send('Server is running');
});

//Websocket client connect to WS
var client = new W3CWebSocket('ws://demos.kaazing.com/echo');

//Websocket event error
client.onerror = function() {
    console.log('Connection Error');
};

//Websocket event open
client.onopen = function() {
    console.log('WebSocket Client Connected');
   
    function sendNumber() {
        if (client.readyState === client.OPEN) {
            var number = Math.round(Math.random() * 0xFFFFFF);
            client.send(number.toString());
            setTimeout(sendNumber, 2000);
        }
    }
    sendNumber();
    
};

//Websocket event close
client.onclose = function() {
    console.log('echo-protocol Client Closed');
};

//Websocket event message received
client.onmessage = function(msg) {

	var response = JSON.parse(msg.data);
	console.log('Received: %s', JSON.stringify(response));

    //Send for all
	io.emit('message', JSON.stringify(response));

};

//Create a server
http.listen(port, function(){
  console.log('Server online on port %s', port);
});