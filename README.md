# Websocket Client with Socket.IO Server

Simple example of connecting to a websocket and sending the data to an HTML page.

# Installation


```sh
$ git clone https://guilhermelirio@bitbucket.org/guilhermelirio/websocket-client-with-socket.io-server.git
$ cd websocket-client-with-socket.io-server
$ npm install
```


# Usage

```sh
$ node index.js
```

Open index.html in favorite browser



# License
----

MIT
